//
//  ScoreTests.swift
//  Da Movie Quizz
//
//  Created by jibeex on 1/27/16.
//  Copyright © 2016 Jianbin LIN. All rights reserved.
//

import XCTest

class ScoreTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
    
    //Score with bigger score value should be before that with lower value
    func testScoreIsBefore(){
        
        let score1 = Score()
        score1.score = 13
        score1.time = Double(arc4random() % 50)
        score1.date = NSDate().dateByAddingTimeInterval(Double(arc4random() % 50))
        
        let score2 = Score()
        score2.score = 12
        score2.time = Double(arc4random() % 50)
        score2.date = NSDate().dateByAddingTimeInterval(Double(arc4random() % 50))
        
        XCTAssertTrue(score1.isHigherThanScore(score2))
        
    }
    
    //If scores have the same score value, the one with smaller time should be before that with bigger time
    func testScoreIsBefore2(){
        
        let score1 = Score()
        score1.score = 12
        score1.time = 8
        score1.date = NSDate().dateByAddingTimeInterval(Double(arc4random() % 50))
        
        let score2 = Score()
        score2.score = 12
        score2.time = 10
        score2.date = NSDate().dateByAddingTimeInterval(Double(arc4random() % 50))
        
        XCTAssertTrue(score1.isHigherThanScore(score2))
        
    }
    
    //If scores have the same score value and time, the one with older date should be before the other
    func testScoreIsBefore3(){
        
        let score1 = Score()
        score1.score = 12
        score1.time = 10
        score1.date = NSDate().dateByAddingTimeInterval(Double(arc4random() % 50))
        
        let score2 = Score()
        score2.score = 12
        score2.time = 10
        score2.date = score1.date.dateByAddingTimeInterval(Double(arc4random() % 50))
        
        XCTAssertTrue(score1.isHigherThanScore(score2))
        
    }
}
