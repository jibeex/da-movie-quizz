# Da Movie Quizz

[Demo video](https://bitbucket.org/jibeex/da-movie-quizz/src/325db691c933719011f65b9f5595d3f80fdfc76e/Demo.mov)

## Online Demo

``https://appetize.io/app/ndn0mpy1n33v8d5ac2xgj8wvk0`` updated:08/02/2016

## Get started

#### Install X-Code

install X-Code 7.2 or above from App Store

``https://itunes.apple.com/fr/app/xcode/id497799835?mt=12``

#### Retrieve code
``git clone https://jibeex@bitbucket.org/jibeex/da-movie-quizz.git``

#### Install CocoaPods(dependency management tool)

``$ sudo gem install cocoapods``

#### Install dependencies

`$ cd <path/to/your/project>` (Go the the project folder)

`$ pod setup` (optional, You may only need this command for the first time)

`$ pod install`(download all dependencies)

`$ pod update` (optional)


#### Open the project

Go to the project folder and open the file ``Da Movie Quizz.xcworkspace``

#### Run the application on simulator

* Choose the right target (which application do you want to build ?)

![](Screenshoots/sceenshot1.png)

* Choose the right simulator (iPhone only)

![](Screenshoots/sceenshot2.png)

* Click on the play button

![](Screenshoots/sceenshot3.png)

## Contributions

Migrate the library JLTMDbClient to AFNetworking 3.0
`https://github.com/JaviLorbada/JLTMDbClient/pulls`