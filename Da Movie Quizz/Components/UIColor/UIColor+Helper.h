//
//  UIColor+Helper.h

#import <UIKit/UIKit.h>

@interface UIColor (Helper)

+ (UIColor *) colorWithHexString: (NSString *) hexString;

+ (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length;

- (UIColor *)lighterColorRemoveSaturation:(CGFloat)removeS
                              resultAlpha:(CGFloat)alpha ;

- (UIColor *)lighterColor;

@end
