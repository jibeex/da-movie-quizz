//
//  GeneralExtensions.swift
//  Taskito
//
//  Created by jibeex on 9/26/15.
//  Copyright © 2015 Modaohoho. All rights reserved.
//

import UIKit

extension Int {
    func format(f: String) -> String {
        return NSString(format: "%\(f)d", self) as String
    }
}

extension Double {
    func format(f: String) -> String {
        return NSString(format: "%\(f)f", self) as String
    }
}

extension Float {
    func format(f: String) -> String {
        return NSString(format: "%\(f)f", self) as String
    }
}

extension UIImageView{
    
    func setImageWithURL(url:String, fadeIn:Bool){
        self.setImageWithURL(url, imageTintColor: nil, fadeIn: fadeIn)
    }
    
    func setImageWithURL(url:String, imageTintColor:UIColor?, fadeIn:Bool){
        
        if fadeIn == true{
            self.alpha = 0
        }
        
        self.setImageWithURL(url, didSucceed: { (request, response, image) -> Void in
            if imageTintColor != nil{
                self.image = image.maskNonTransparentPartOfImageWithColor(imageTintColor)
            }
            else{
                self.image = image
            }
            
            if fadeIn == true{
                UIView.animateWithDuration(0.4, animations: { () -> Void in
                    self.alpha = 1
                })
            }
            
            }, didFail: nil);
        
    }
    
    func setImageWithURL(url:String, didSucceed:((request:NSURLRequest, response:NSHTTPURLResponse?, image:UIImage) ->Void)?,
        didFail:((request:NSURLRequest?, response:NSHTTPURLResponse?, error:NSError?)->Void)?){
            
            self.image = nil
            
            let urlIsNotValidError = NSError(domain: "app", code: 2, userInfo: [NSLocalizedDescriptionKey : Localized("app_error_2")])
            
            guard let adjustedUrl = url.stringByRemovingPercentEncoding else{
                didFail?(request: nil, response: nil, error: urlIsNotValidError)
                return
            }
            
            guard let urlString = adjustedUrl.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet()) else{
                didFail?(request: nil, response: nil, error: urlIsNotValidError)
                return
            }
            
            guard let url = NSURL(string:urlString) else{
                didFail?(request: nil, response: nil, error: urlIsNotValidError)
                return
            }
            
            
            self.setImageWithURLRequest(NSURLRequest(URL: url), placeholderImage: nil, success: didSucceed, failure: didFail)
    }
}

extension UIView {
    
    private struct AssociatedKey {
        static var DisabledConstraints = "disabledConstraints"
        static var ShouldDisableConstraintsWhenViewIsHidden = "ShouldDisableConstraintsWhenViewIsHidden"
    }
    
    func vibrate(offset:CGFloat, duration:CFTimeInterval, repeatCount:Float){
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = duration
        animation.repeatCount = repeatCount
        animation.autoreverses = true
        animation.fromValue = NSValue(CGPoint: CGPointMake(self.center.x - offset, self.center.y))
        animation.toValue = NSValue(CGPoint: CGPointMake(self.center.x + offset, self.center.y))
        self.layer.addAnimation(animation, forKey: "position")
    }
    
    func takeSnapshot() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, UIScreen.mainScreen().scale)
        
        drawViewHierarchyInRect(self.bounds, afterScreenUpdates: true)
        
        // old style: layer.renderInContext(UIGraphicsGetCurrentContext())
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        return image
    }
}

