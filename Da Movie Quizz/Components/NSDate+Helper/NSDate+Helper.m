//
//  NSDate+Category.m
//
//
//  Created by Youri on 08.08.12.
//  Copyright (c) 2012 MyCompanyName. All rights reserved.
//

#import "NSDate+Helper.h"
#import <ISO8601DateFormatter/ISO8601DateFormatter.h>

NSString* defaultLocaleIdentifier = nil;

@implementation NSDate (Helper)

#define kCalendar [NSDate currentCalendar]

#define kDateComponents (NSYearCalendarUnit| NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitWeekday | NSCalendarUnitWeekOfMonth | NSCalendarUnitWeekOfYear)

#pragma mark -
#pragma mark Comparing dates

+(void)setDefaultLocaleIdentifier:(NSString*) identifier{
    defaultLocaleIdentifier = identifier;
}

- (BOOL) isBeforeDate: (NSDate *) aDate
{
    if(aDate == nil){
        return NO;
    }
    return [self compare:aDate] == NSOrderedAscending;
}

- (BOOL) isAfterDate: (NSDate *) aDate
{
    if(aDate == nil){
        return NO;
    }
    return [self compare:aDate] == NSOrderedDescending;
}

- (BOOL) isBetweenStartDate:(NSDate*)start andEndDate:(NSDate*)end inclusive:(BOOL)isInclusive{
    
    if (isInclusive) {
        if (![self isBeforeDate:start] && ![self isAfterDate:end]) {
            return YES;
        }
    } else{
        if ([self isAfterDate:start] && [self isBeforeDate:end]) {
            return YES;
        }
    }
    return NO;
}

- (BOOL)isSameDayWithDate:(NSDate*)date {
    
    if(date == nil){
        return NO;
    }
    
    NSCalendar* calendar = kCalendar;
    
    //DDLogVerbose(@"Calendar time zone:%@", kCalendar.timeZone.name);
    
    @synchronized([NSCalendar class]){
        unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
        
        NSDateComponents* comp1 = [calendar components:unitFlags fromDate:self];
        NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date];
        
        BOOL ret = [comp1 day]   == [comp2 day] &&
        [comp1 month] == [comp2 month] &&
        [comp1 year]  == [comp2 year];
        
        return ret;
    }
}

- (BOOL)isSameMonthAsDate:(NSDate *)date
{
    NSDateComponents *beginComponents = [kCalendar components:NSCalendarUnitMonth fromDate:self];
    NSInteger beginMonth = [beginComponents month];
    
    NSDateComponents *endComponents = [kCalendar components:NSCalendarUnitMonth fromDate:date];
    NSInteger endMonth = [endComponents month];
    
    return beginMonth == endMonth;
}

#pragma mark -
#pragma mark Date formatting

- (NSString*)localeFormattedDateString {
    
    NSDateFormatter *formatter = [NSDate threadDateFormatter];
    [formatter setDateStyle:NSDateFormatterLongStyle];
    [formatter setTimeStyle:NSDateFormatterNoStyle];
    
    NSString *ret = [formatter stringFromDate:self];
    
    return ret;
}

- (NSString*)localeFormattedDateStringWithTime {
    
    NSDateFormatter *formatter = [NSDate threadDateFormatter];
    [formatter setDateFormat:@"MMM dd, yyyy HH:mm"];
    //   [formatter setDateStyle:NSDateFormatterShortStyle];
    NSString *ret = [formatter stringFromDate:self];
    return ret;
}

+ (NSDate *)localeFormatted {
    
    return [[NSDate date] dateFormattedLocale];
}

- (NSDate *)dateFormattedLocale {
    
    NSDateFormatter *formatter = [NSDate threadDateFormatter];
    [formatter setTimeStyle:NSDateFormatterNoStyle];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    NSString *ret = [formatter stringFromDate:self];
    
    return [formatter dateFromString:ret];
}


- (NSString *)formattedStringWithFormat:(NSString *)format
{
    NSDateFormatter *formatter = [NSDate threadDateFormatter];
    [formatter setDateFormat:format];
    NSString *ret = [formatter stringFromDate:self];
    
    return ret;
}

- (NSString*)formattedStringWithFormatWithFormat:(NSString *)dateFormat forTimeZone:(NSTimeZone*)timeZone{
    
    NSDateFormatter* dateFormatter = [NSDate threadDateFormatter];
    
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:dateFormat];
    return [dateFormatter stringFromDate:self];
}

+ (NSString*)stringFromTimeInterval:(NSTimeInterval)duration
{
    NSInteger minutes = floor(duration/60);
    NSInteger seconds = round(duration - minutes * 60);
    return [NSString stringWithFormat:@"%d:%02d", (int)minutes, (int)seconds];
}

-(NSString*)stringFromTimeStampInMillionSeconds{
    
    return [NSString stringWithFormat:@"%.0lf", [self timeIntervalSince1970] * 1000.0f];
    
}

- (NSString*)stringWithoutTimeForTimeZone:(NSTimeZone*)timeZone{
    
    NSDateFormatter* dateFormatter = [NSDate threadDateFormatter];
    
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    return [dateFormatter stringFromDate:self];
}


- (NSDate *)dateWithoutTime
{
    
    NSDateFormatter *formatter = [NSDate threadDateFormatter];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    NSString *ret = [formatter stringFromDate:self];
    
    return [formatter dateFromString:ret];
}

+ (NSDate *)dateWithoutTime
{
    return [[NSDate date] dateWithoutTime];
}


#pragma mark -
#pragma mark SQLite formatting

- (NSDate *) dateForSqlite {
    NSDateFormatter *formatter = [NSDate threadDateFormatter];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString *ret = [formatter stringFromDate:self];
    
    NSDate *date = [formatter dateFromString:ret];
    
    return date;
}

+ (NSDate*) dateFromSQLString:(NSString*)dateStr {
    
    NSDateFormatter *dateForm = [NSDate threadDateFormatter];
    [dateForm setDateFormat:@"yyyy-MM-dd HH:mm:ss ZZZ"];
    NSDate *date = [dateForm dateFromString:dateStr];
    return date;
}


#pragma mark -
#pragma mark Beginning and end of date components

- (NSDate *) startOfDay
{
    
    
    NSDateComponents *components = [kCalendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit)
                                                fromDate:self];
    [components setHour: 0];
    [components setMinute: 0];
    [components setSecond: 0];
    
    return [kCalendar dateFromComponents:components];
}

- (NSDate *) endOfDay{
    NSDateComponents *components = [kCalendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit)
                                                fromDate:self];
    [components setHour: 23];
    [components setMinute: 59];
    [components setSecond: 59];
    
    return [kCalendar dateFromComponents:components];
}


- (NSDate *)startOfWeek {
    
    NSDate *beginningOfWeek = nil;
    BOOL ok = [kCalendar rangeOfUnit:NSWeekCalendarUnit startDate:&beginningOfWeek
                            interval:NULL forDate:self];
    if (ok) {
        return beginningOfWeek;
    }
    
    // couldn't calc via range, so try to grab Sunday, assuming gregorian style
    // Get the weekday component of the current date
    NSDateComponents *weekdayComponents = [kCalendar components:NSWeekdayCalendarUnit fromDate:self];
    
    /*
     Create a date components to represent the number of days to subtract from the current date.
     The weekday value for Sunday in the Gregorian calendar is 1, so subtract 1 from the number of days to subtract from the date in question.  (If today's Sunday, subtract 0 days.) */
    
    NSDateComponents *componentsToSubtract = [[NSDateComponents alloc] init];
    
    [componentsToSubtract setDay: 0 - ([weekdayComponents weekday] + 1)];
    beginningOfWeek = nil;
    beginningOfWeek = [kCalendar dateByAddingComponents:componentsToSubtract toDate:self options:0];
    
    //normalize to midnight, extract the year, month, and day components and create a new date from those components.
    NSDateComponents *components = [kCalendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit)
                                                fromDate:beginningOfWeek];
    return [kCalendar dateFromComponents:components];
    
}

- (NSDate *)startOfMonth {
    
    NSCalendar* calendar = kCalendar;
    @synchronized([NSCalendar class]){
        NSDateComponents *comp = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:self];
        [comp setDay:1];
        NSDate *firstDayOfMonthDate = [calendar dateFromComponents:comp];
        return firstDayOfMonthDate;
    }
    
}

- (NSDate *)startOfYear {
    
    NSCalendar* calendar = kCalendar;
    
    @synchronized([NSCalendar class]){
        NSDateComponents *comp = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:self];
        [comp setDay:1];
        [comp setMonth:1];
        NSDate *firstDayOfYearDate = [calendar dateFromComponents:comp];
        return firstDayOfYearDate;
    }
    
}

- (NSDate *)endOfWeek {
    NSDateComponents *weekdayComponents = [kCalendar components:NSWeekdayCalendarUnit fromDate:self];
    NSDateComponents *componentsToAdd = [[NSDateComponents alloc] init];
    NSInteger weekday = [weekdayComponents weekday];
    
    [componentsToAdd setDay:(8 - weekday) % 7];
    NSDate *endOfWeek = [kCalendar dateByAddingComponents:componentsToAdd toDate:self options:0];
    
    return endOfWeek;
}

- (NSDate *)endOfMonth {

    NSDateComponents* comps = [kCalendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSWeekCalendarUnit|NSWeekdayCalendarUnit fromDate:self]; // Get necessary date components
    // set last of month
    [comps setMonth:[comps month]+1];
    [comps setDay:0];
    
    return [[kCalendar dateFromComponents:comps] endOfDay];
}

- (NSDate *)startOfQuarter {
    
    NSDate* monthOfEndOfQuarter;
    if (self.month <= 3) {
        monthOfEndOfQuarter = [NSDate dateByComposingDay:self.day month:1 year:self.year];
    }
    else if(self.month <= 6){
        monthOfEndOfQuarter = [NSDate dateByComposingDay:self.day month:4 year:self.year];
    }
    else if(self.month <= 9){
        monthOfEndOfQuarter = [NSDate dateByComposingDay:self.day month:7 year:self.year];
    }
    else{
        monthOfEndOfQuarter = [NSDate dateByComposingDay:self.day month:10 year:self.year];
    }
    return [monthOfEndOfQuarter startOfMonth];
}

- (NSDate *)endOfQuarter {
    
    NSDate* monthOfEndOfQuarter;
    if (self.month <= 3) {
        monthOfEndOfQuarter = [NSDate dateByComposingDay:self.day month:3 year:self.year];
    }
    else if(self.month <= 6){
        monthOfEndOfQuarter = [NSDate dateByComposingDay:self.day month:6 year:self.year];
    }
    else if(self.month <= 9){
        monthOfEndOfQuarter = [NSDate dateByComposingDay:self.day month:9 year:self.year];
    }
    else{
        monthOfEndOfQuarter = [NSDate dateByComposingDay:self.day month:12 year:self.year];
    }
    return [monthOfEndOfQuarter endOfMonth];
}

- (NSDate *)endOfYear {
    
    NSUInteger days = 0;
    NSDateComponents *components = [kCalendar components:NSYearCalendarUnit fromDate:self];
    NSUInteger months = [kCalendar rangeOfUnit:NSMonthCalendarUnit
                                        inUnit:NSYearCalendarUnit
                                       forDate:self].length;
    for (int i = 1; i <= months; i++) {
        components.month = i;
        NSDate *month = [kCalendar dateFromComponents:components];
        days += [kCalendar rangeOfUnit:NSDayCalendarUnit
                                inUnit:NSMonthCalendarUnit
                               forDate:month].length;
    }
    
    NSDateComponents *comps = [kCalendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:self];;
    
    [comps setMonth:12];
    
    return [[kCalendar dateFromComponents:comps] endOfMonth];
}

-(NSDate*)mondayOfWeek{
    
    NSCalendar* calendar = kCalendar;
    @synchronized([NSCalendar class]){
        NSDateComponents* comps = [calendar components:NSYearForWeekOfYearCalendarUnit |NSYearCalendarUnit|NSMonthCalendarUnit|NSWeekCalendarUnit|NSWeekdayCalendarUnit fromDate:self];
        [comps setWeekday:2]; // 2: monday
        NSDate* firstDayOfTheWeek = [calendar dateFromComponents:comps];
        return firstDayOfTheWeek;
    }
    
}

-(NSDate*)sundayOfWeek{
    NSCalendar* calendar = kCalendar;
    @synchronized([NSCalendar class]){
        NSDateComponents* comps = [calendar components:NSYearForWeekOfYearCalendarUnit |NSYearCalendarUnit|NSMonthCalendarUnit|NSWeekCalendarUnit|NSWeekdayCalendarUnit fromDate:self];
        
        [comps setWeekday:1]; // 1: sunday
        NSDate* firstDayOfTheWeek = [calendar dateFromComponents:comps];
        return firstDayOfTheWeek;
    }
}


#pragma mark -
#pragma mark Date math

- (NSDate *) dateByAddingDays:(NSInteger)days
{
    NSDate *date = [self dateByAddingTimeInterval:(days * kSecondsDay)];
    return date;
}

- (NSDate *) dateBySubtractingDays:(NSInteger)days
{
    
    NSDate *date = [self dateByAddingTimeInterval:(-days * kSecondsDay)];
    return date;
}

- (NSDate *) dateByAddingHours:(NSInteger)hours
{
    NSDate *date = [self dateByAddingTimeInterval:(hours * kSecondsHour)];
    return date;
}

- (NSDate *) dateBySubtractingHours:(NSInteger)hours
{
    NSDate *date = [self dateByAddingTimeInterval:(-hours * kSecondsHour)];
    return date;
}

- (NSDate *) dateByAddingMinutes:(NSInteger)minutes
{
    NSDate *date = [self dateByAddingTimeInterval:(minutes * kSecondsMinute)];
    return date;
}

- (NSDate *) dateBySubtractingMinutes:(NSInteger)minutes
{
    NSDate *date = [self dateByAddingTimeInterval:(-minutes * kSecondsMinute)];
    return date;
}


- (NSDate*) dateByAddingMonths:(NSInteger)months
{
    return [self dateBySubstractingMonths:- months];
}

- (NSDate*) dateBySubstractingMonths:(NSInteger)months
{
    NSDate *today = self;
    NSCalendar* calendar = kCalendar;
    
    @synchronized([NSCalendar class]){
        NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
        [offsetComponents setMonth:-1 * months]; // note that I'm setting it to -1
        NSDate *newDate = [calendar dateByAddingComponents:offsetComponents toDate:today options:0];
        return newDate;
    }
    
}

-(NSDate*)dateByAddingYears:(NSInteger)yearNumber{
    
    NSDate *today = self;
    NSCalendar* calendar = kCalendar;
    
    @synchronized([NSCalendar class]){
        NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
        [offsetComponents setYear: yearNumber];
        
        NSDate *newDate = [calendar dateByAddingComponents:offsetComponents toDate:today options:0];
        return newDate;
    }
}

-(NSDate*)dateBySubstractingYears:(NSInteger)yearNumber{
    
    return [self dateByAddingYears:-yearNumber];
}



#pragma mark Date components

- (NSInteger) hour
{
    NSDateComponents *components = [kCalendar components:kDateComponents fromDate:self];
    return [components hour];
}

- (NSInteger) minute
{
    NSDateComponents *components = [kCalendar components:kDateComponents fromDate:self];
    return [components minute];
}

- (NSInteger) seconds
{
    NSDateComponents *components = [kCalendar components:kDateComponents fromDate:self];
    return [components second];
}

- (NSInteger) day
{
    NSDateComponents *components = [kCalendar components:kDateComponents fromDate:self];
    return [components day];
}

- (NSInteger) month
{
    NSDateComponents *components = [kCalendar components:kDateComponents fromDate:self];
    return [components month];
}

- (NSInteger) weekOfYear
{
    NSDateComponents *components = [kCalendar components:kDateComponents fromDate:self];
    return [components weekOfYear];
}

- (NSInteger) weekday
{
    NSDateComponents *components = [kCalendar components:kDateComponents fromDate:self];
    return [components weekday];
}

- (NSInteger) nthWeekday // e.g. 2nd Tuesday of the month is 2
{
    NSDateComponents *components = [kCalendar components:kDateComponents fromDate:self];
    return [components weekdayOrdinal];
}

- (NSInteger) quarter{
    return ([self month]-1)/3+1;
}

- (NSInteger) year
{
    NSDateComponents *components = [kCalendar components:kDateComponents fromDate:self];
    return [components year];
}

+(NSInteger)ageOfWhoWithBirthdate:(NSDate *)dateOfBirth {
    NSCalendar *calendar = kCalendar;
    @synchronized([NSCalendar class]){
        unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
        NSDateComponents *dateComponentsNow = [calendar components:unitFlags fromDate:[NSDate date]];
        NSDateComponents *dateComponentsBirth = [calendar components:unitFlags fromDate:dateOfBirth];
        
        if (([dateComponentsNow month] < [dateComponentsBirth month]) ||
            (([dateComponentsNow month] == [dateComponentsBirth month]) && ([dateComponentsNow day] < [dateComponentsBirth day]))) {
            return [dateComponentsNow year] - [dateComponentsBirth year] - 1;
        } else {
            return [dateComponentsNow year] - [dateComponentsBirth year];
        }
    }
}

- (NSString*) monthName {
    
    NSDateFormatter *formatter = [NSDate threadDateFormatter];
    [formatter setDateFormat:@"MMMM"];
    
    NSString *stringFromDate = [formatter stringFromDate:self];
    
    return stringFromDate;
}

- (NSString*) yearFromDateStr {
    
    NSDateFormatter *formatter = [NSDate threadDateFormatter];
    [formatter setDateFormat:@"YYYY"];
    
    NSString *stringFromDate = [formatter stringFromDate:self];
    
    return stringFromDate;
}

+ (NSDate*)dateByComposingDayFrom:(NSDate*)day timeFrom:(NSDate*)time{
    
    NSCalendar* calendar = kCalendar;
    @synchronized([NSCalendar class]){
        NSDateComponents *dayComponents = [calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit)  fromDate:day];
        
        NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:time];
        
        [dayComponents setHour:[timeComponents hour]];
        [dayComponents setMinute:[timeComponents minute]];
        [dayComponents setSecond:[timeComponents second]];
        
        NSDate *newDate = [calendar dateFromComponents:dayComponents];
        //DDLogVerbose(@"To compose date with day from:%@ with time from:%@ Result:%@", day, time, newDate);
        return newDate;
    }
    
}

+ (NSDate*)dateByComposingDay:(NSInteger)day month:(NSInteger)month year:(NSInteger)year{
    
    @synchronized([NSCalendar class]){
        NSDateComponents *comps = [[NSDateComponents alloc] init];
        [comps setDay:day];
        [comps setMonth:month];
        [comps setYear:year];
        return [[NSCalendar currentCalendar] dateFromComponents:comps];
    }
}

#pragma mark Date from string

+ (NSDate *)dateFromString:(NSString *)string
{
    if (string)
    {
        ISO8601DateFormatter *formatter = [[ISO8601DateFormatter alloc] init];
        NSDate *theDate = [formatter dateFromString:string];
        return theDate;
    }
    return nil;
}

/*
 + (NSDate*)dateFromString:(NSString*)dateString{
 
 NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Europe/Paris"];
 return [self dateFromString:dateString forTimeZone:timeZone];
 
 }
 
 + (NSDate*)dateFromString:(NSString*)dateString forTimeZone:(NSTimeZone*)timeZone{
 
 NSDateFormatter* dateFormatter = [self threadDateFormatter];
 
 [dateFormatter setTimeZone:timeZone];
 
 NSUInteger numberOfColons = [Utils numberOfOccurrenceOfString:@":" inString:dateString];
 NSUInteger numberOfBackSlashes = [Utils numberOfOccurrenceOfString:@"/" inString:dateString];
 NSUInteger numberOfHyphens = [Utils numberOfOccurrenceOfString:@"-" inString:dateString];
 
 if (numberOfBackSlashes > 0){
 
 if (numberOfColons == 0){
 [dateFormatter setDateFormat:@"dd/MM/yyyy"];
 }
 else if (numberOfColons == 1){
 [dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm"];
 }
 else{
 [dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
 }
 
 }
 else if(numberOfHyphens > 0){
 
 if (numberOfColons == 0){
 [dateFormatter setDateFormat:@"dd-MM-yyyy"];
 }
 else if (numberOfColons == 1){
 [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm"];
 }
 else{
 [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
 }
 
 
 }
 else{
 
 NSUInteger numberOfComma = [Utils numberOfOccurrenceOfString:@"," inString:dateString];
 NSRange prefixRangeOfAm = [dateString rangeOfString:@"am" options:(NSAnchoredSearch | NSCaseInsensitiveSearch)];
 NSRange prefixRangeOfPm = [dateString rangeOfString:@"pm" options:(NSAnchoredSearch | NSCaseInsensitiveSearch)];
 BOOL hasAmOrPm = prefixRangeOfAm.location != NSNotFound || prefixRangeOfPm.location != NSNotFound;
 
 if(numberOfComma > 0){
 
 if(hasAmOrPm){
 [dateFormatter setDateFormat:@"EEE, dd MMM yyyy hh:mm:ss a Z"];
 }
 else{
 [dateFormatter setDateFormat:@"EEE, dd MMM yyyy HH:mm:ss Z"];
 }
 
 NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
 [dateFormatter setLocale:usLocale];
 }
 else{
 
 if (numberOfColons == 1){
 [dateFormatter setDateFormat:@"HH:mm"];
 }
 else{
 [dateFormatter setDateFormat:@"HH:mm:ss"];
 }
 
 }
 
 
 }
 
 
 
 return [dateFormatter dateFromString:dateString];
 }
 */

+ (NSDate*)dateFromString:(NSString*)dateString withFormat:(NSString*)format forTimeZone:(NSTimeZone*)timeZone{
    
    NSDateFormatter* dateFormatter = [self threadDateFormatter];
    
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:format];
    
    return [dateFormatter dateFromString:dateString];
}

#pragma mark Auxiliary methods

+ (NSMutableDictionary*)threadDictionary{
    return [[NSThread currentThread]threadDictionary];
}

+ (NSLocale*)currentLocaleWithLocaleIdentifier:(NSString*)identifier{
    
    static const NSString* key = @"threadCurrentLocale";
    
    NSMutableDictionary* dict = [self threadDictionary];
    
    NSLocale* ret = [dict objectForKey:key];
    
    if(ret == nil){
        
        @synchronized([NSLocale class]){
            ret = [[NSLocale alloc] initWithLocaleIdentifier:identifier];
        }
        NSAssert(ret != nil, @"Locale identifier doesn't exist:%@", defaultLocaleIdentifier);
        //DDLogVerbose(@"Locale identifier:%@ %p", ret.localeIdentifier, ret);
        [dict setObject:ret forKey:key];
    }
    
    return ret;
    
}

+(NSDateFormatter*)threadDateFormatter{
    
    return [self threadDateFormatterWithLocaleIdentifier:defaultLocaleIdentifier];
}

+(NSDateFormatter*)threadDateFormatterWithLocaleIdentifier:(NSString*)identifier{
    
    static const NSString* key = @"threadDateFormatter";
    
    NSMutableDictionary* dict = [self threadDictionary];
    
    NSDateFormatter* ret = [dict objectForKey:key];
    
    if(ret == nil){
        @synchronized([NSDateFormatter class]){
            ret = [[NSDateFormatter alloc] init];
        }
        NSLocale* locale = [self currentLocaleWithLocaleIdentifier:identifier];
        [ret setLocale:locale];
        [ret setCalendar:[NSDate currentCalendar]];
        [dict setObject:ret forKey:key];
        
    }
    
    return ret;
}

+(NSDateFormatter*)threadDateFormatterWithDateFormat:(NSString*) dateFormat{
    
    NSDateFormatter* dateFormatter = [self threadDateFormatter];
    dateFormatter.dateFormat = dateFormat;
    return dateFormatter;
}

+ (NSCalendar*)currentCalendar{
    @synchronized([NSCalendar class]){
        NSCalendar* calendar = [NSCalendar currentCalendar];
        [calendar setFirstWeekday:2];
        return calendar;
    }
}

@end
