//
//  NSDate+Category.h
//
//
//  Created by Youri on 08.08.12.
//  Copyright (c) 2012 MyCompanyName. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kSecondsMinute      60
#define kSecondsHour		3600
#define kSecondsDay         86400
#define kSecondsWeek		604800

extern NSString* defaultLocaleIdentifier;

@interface NSDate (Helper)

+(NSDateFormatter*)threadDateFormatter;
+(NSDateFormatter*)threadDateFormatterWithDateFormat:(NSString*) dateFormat;
+(NSCalendar*)currentCalendar;
+(void)setDefaultLocaleIdentifier:(NSString*) identifier;

#pragma mark Comparing dates

- (BOOL) isBeforeDate: (NSDate *) aDate;
- (BOOL) isAfterDate: (NSDate *) aDate;
- (BOOL) isBetweenStartDate:(NSDate*)start andEndDate:(NSDate*)end inclusive:(BOOL)isInclusive;

/**
 * Check if two NSDate objects are in the same day
 * @return true if in the same day
 *         false otherwise
 *
 */
- (BOOL)isSameDayWithDate:(NSDate*)date;
- (BOOL)isSameMonthAsDate:(NSDate *)date;

#pragma mark Date formatting

- (NSString*)localeFormattedDateString;
- (NSString*)localeFormattedDateStringWithTime;
+ (NSDate *)localeFormatted;
- (NSDate *)dateFormattedLocale;
+ (NSString*)stringFromTimeInterval:(NSTimeInterval)duration;
-(NSString*)stringFromTimeStampInMillionSeconds;
- (NSString*)stringWithoutTimeForTimeZone:(NSTimeZone*)timeZone;


- (NSString *)formattedStringWithFormat:(NSString *)format;
/**
 * Return datetime string in format given in |dateFormat| from the given |time| and |timeZone|
 */
- (NSString*)formattedStringWithFormatWithFormat:(NSString *)dateFormat forTimeZone:(NSTimeZone*)timeZone;
- (NSDate *)dateWithoutTime;
+ (NSDate *)dateWithoutTime;

#pragma mark SQLite formatting

- (NSDate *) dateForSqlite;
+ (NSDate*) dateFromSQLString:(NSString*)dateStr;

#pragma mark Beginning and end of date components

- (NSDate *) startOfDay;

- (NSDate *) endOfDay;

- (NSDate *)startOfWeek;

- (NSDate *)endOfWeek;

- (NSDate *)startOfMonth;

- (NSDate *)endOfMonth;

- (NSDate *)startOfYear;

- (NSDate *)endOfYear;

- (NSDate *)startOfQuarter;

- (NSDate *)endOfQuarter;

- (NSDate*)mondayOfWeek;

- (NSDate*)sundayOfWeek;

#pragma mark Date math

- (NSDate *) dateByAddingMinutes:(NSInteger)minutes;

- (NSDate *) dateBySubtractingMinutes:(NSInteger)minutes;

- (NSDate *) dateByAddingHours:(NSInteger)hours;

- (NSDate *) dateBySubtractingHours:(NSInteger)hours;

- (NSDate *) dateByAddingDays:(NSInteger)days;

- (NSDate *) dateBySubtractingDays:(NSInteger)days;

- (NSDate *) dateByAddingMonths:(NSInteger)months;

- (NSDate *) dateBySubstractingMonths:(NSInteger)months;

- (NSDate*)dateByAddingYears:(NSInteger)yearNumber;

- (NSDate*)dateBySubstractingYears:(NSInteger)yearNumber;

#pragma mark Date components

- (NSInteger) seconds;

- (NSInteger) minute;

- (NSInteger) hour;

- (NSInteger) day;

- (NSInteger) month;

- (NSInteger) weekOfYear;

- (NSInteger) weekday;

- (NSInteger) quarter;

- (NSInteger) year;

+(NSInteger)ageOfWhoWithBirthdate:(NSDate *)dateOfBirth;

- (NSString*) monthName;

- (NSString*) yearFromDateStr;

/**
 * Compose an NSDate object with the day, month and year from |day| and hour, minute and seconds
 * from |time|
 *
 */
+ (NSDate*)dateByComposingDayFrom:(NSDate*)day timeFrom:(NSDate*)time;

+ (NSDate*)dateByComposingDay:(NSInteger)day month:(NSInteger)month year:(NSInteger)year;

+ (NSDate*)dateFromString:(NSString*)dateString;

//+ (NSDate*)dateFromString:(NSString*)dateString forTimeZone:(NSTimeZone*)timeZone;

+ (NSDate*)dateFromString:(NSString*)dateString withFormat:(NSString*)format forTimeZone:(NSTimeZone*)timeZone;


@end
