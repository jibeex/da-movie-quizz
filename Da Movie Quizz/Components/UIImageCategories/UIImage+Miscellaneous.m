//
//  UIImage+CaptainDash.m
//  CaptainDash
//
//  Created by Emilien on 8/23/12.
//  Copyright (c) 2012 CaptainDash. All rights reserved.
//

#import "UIImage+Miscellaneous.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIImage (Miscellaneous)

+ (NSString *)splashImageName{
    
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    
    CGSize viewSize = [[UIScreen mainScreen] bounds].size;
    NSString* viewOrientation = @"Portrait";
    if (UIDeviceOrientationIsLandscape(orientation)) {
        viewSize = CGSizeMake(viewSize.height, viewSize.width);
        viewOrientation = @"Landscape";
    }
    
    NSArray* imagesDict = [[[NSBundle mainBundle] infoDictionary] valueForKey:@"UILaunchImages"];
    for (NSDictionary* dict in imagesDict) {
        CGSize imageSize = CGSizeFromString(dict[@"UILaunchImageSize"]);
        if (CGSizeEqualToSize(imageSize, viewSize) && [viewOrientation isEqualToString:dict[@"UILaunchImageOrientation"]])
            return dict[@"UILaunchImageName"];
    }
    return nil;
}

+ (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0, 0, 1, 1);
    
    // create a 1 by 1 pixel context
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
    [color setFill];
    UIRectFill(rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

/**
 * Tint an image
 * See http://stackoverflow.com/questions/3514066/how-to-tint-a-transparent-png-image-in-iphone
 */
-(UIImage *)colorizeWhitePartOfImageWithColor:(UIColor *)tintColor {
    
    UIGraphicsBeginImageContextWithOptions(self.size, NO,[[UIScreen mainScreen] scale]); // for correct resolution on retina
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect rect = CGRectMake(0, 0, self.size.width, self.size.height);
    CGContextScaleCTM(context, 1, -1);
    CGContextTranslateCTM(context, 0, -rect.size.height);
    
    //Image drawing code
    CGContextSaveGState(context);
    CGContextClipToMask(context, rect, self.CGImage);
    [tintColor set];
    CGContextFillRect(context, rect);
    CGContextRestoreGState(context);
    CGContextSetBlendMode(context, kCGBlendModeMultiply);
    CGContextDrawImage(context, rect, self.CGImage);
    
    //Save the image
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

/**
 * Tint an image
 * See http://stackoverflow.com/questions/3514066/how-to-tint-a-transparent-png-image-in-iphone
 */
-(UIImage *)colorizeTransparentImageWithColor:(UIColor *)tintColor {
    
    UIGraphicsBeginImageContextWithOptions(self.size, NO,[[UIScreen mainScreen] scale]); // for correct resolution on retina
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect rect = CGRectMake(0, 0, self.size.width, self.size.height);
    CGContextScaleCTM(context, 1, -1);
    CGContextTranslateCTM(context, 0, -rect.size.height);
    
    //Image drawing code
    
    // draw black background to preserve color of transparent pixels
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    [[UIColor blackColor] setFill];
    CGContextFillRect(context, rect);
    
    // draw original image
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGContextDrawImage(context, rect, self.CGImage);
    
    // tint image (loosing alpha) - the luminosity of the original image is preserved
    CGContextSetBlendMode(context, kCGBlendModeColor);
    [tintColor setFill];
    CGContextFillRect(context, rect);
    
    // mask by alpha values of original image
    CGContextSetBlendMode(context, kCGBlendModeDestinationIn);
    CGContextDrawImage(context, rect, self.CGImage);
    
    //Save the image
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

/**
 * Tint an image
 * See http://stackoverflow.com/questions/3514066/how-to-tint-a-transparent-png-image-in-iphone
 */
-(UIImage *)colorizeNonTransparentImageWithColor:(UIColor *)tintColor {
    
    UIGraphicsBeginImageContextWithOptions(self.size, NO,[[UIScreen mainScreen] scale]); // for correct resolution on retina
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect rect = CGRectMake(0, 0, self.size.width, self.size.height);
    CGContextScaleCTM(context, 1, -1);
    CGContextTranslateCTM(context, 0, -rect.size.height);
    
    //Image drawing code
    
    // draw tint color
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    [tintColor setFill];
    CGContextFillRect(context, rect);
    
    // replace luminosity of background (ignoring alpha)
    CGContextSetBlendMode(context, kCGBlendModeLuminosity);
    CGContextDrawImage(context, rect, self.CGImage);
    
    // mask by alpha values of original image
    CGContextSetBlendMode(context, kCGBlendModeDestinationIn);
    CGContextDrawImage(context, rect, self.CGImage);
    
    //Save the image
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

/**
 * Tint an image
 * See http://stackoverflow.com/questions/3514066/how-to-tint-a-transparent-png-image-in-iphone
 */
-(UIImage *)colorizeAlphaChannelOfImageWithColor:(UIColor *)tintColor {
    
    UIGraphicsBeginImageContextWithOptions(self.size, NO,[[UIScreen mainScreen] scale]); // for correct resolution on retina
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect rect = CGRectMake(0, 0, self.size.width, self.size.height);
    CGContextScaleCTM(context, 1, -1);
    CGContextTranslateCTM(context, 0, -rect.size.height);
    
    //Image drawing code
    
    // draw tint color
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    [tintColor setFill];
    CGContextFillRect(context, rect);
    
    // mask by alpha values of original image
    CGContextSetBlendMode(context, kCGBlendModeDestinationIn);
    CGContextDrawImage(context, rect, self.CGImage);
    
    //Save the image
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

/**
 * Tint an image
 * See http://stackoverflow.com/questions/3514066/how-to-tint-a-transparent-png-image-in-iphone
 */
-(UIImage *)colorizeNonAlphaChannelOfImageWithColor:(UIColor *)tintColor {
    
    UIGraphicsBeginImageContextWithOptions(self.size, NO,[[UIScreen mainScreen] scale]); // for correct resolution on retina
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect rect = CGRectMake(0, 0, self.size.width, self.size.height);
    CGContextScaleCTM(context, 1, -1);
    CGContextTranslateCTM(context, 0, -rect.size.height);
    
    //Image drawing code
    
    // draw alpha-mask
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGContextDrawImage(context, rect, self.CGImage);
    
    // draw tint color, preserving alpha values of original image
    CGContextSetBlendMode(context, kCGBlendModeSourceIn);
    [tintColor setFill];
    CGContextFillRect(context, rect);
    
    //Save the image
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(UIImage*)maskNonTransparentPartOfImageWithColor:(UIColor *)color{
    return [UIImage maskImage:self withColor:color];
}

+ (UIImage*)maskImage:(UIImage *)img withColor:(UIColor *)color
{
    CGRect imageRect = CGRectMake(0, 0, CGImageGetWidth(img.CGImage), CGImageGetHeight(img.CGImage));
    CGContextRef context = CGBitmapContextCreate(NULL, imageRect.size.width, imageRect.size.height, 8, 0, CGImageGetColorSpace(img.CGImage), kCGBitmapAlphaInfoMask & kCGImageAlphaPremultipliedLast);
    
    if (context != NULL)
    {
        CGContextClipToMask(context, imageRect, img.CGImage);
        CGContextSetFillColorWithColor(context, color.CGColor);
        CGContextFillRect(context, imageRect);
        
        CGImageRef newCGImage = CGBitmapContextCreateImage(context);
        UIImage* newImage = [UIImage imageWithCGImage:newCGImage scale:img.scale orientation:img.imageOrientation];
        
        CGContextRelease(context);
        CGImageRelease(newCGImage);
        
        return newImage;
    }
    else
        return img;
}

+ (UIImage *)imageWithSolidColor:(UIColor *)color
{
    CGRect imageRect = CGRectMake(0, 0, 2, 2);
    CGContextRef context = CGBitmapContextCreate(NULL, imageRect.size.width, imageRect.size.height, 8, 0, CGColorSpaceCreateDeviceRGB(), kCGBitmapAlphaInfoMask & kCGImageAlphaPremultipliedLast);
    
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillRect(context, imageRect);
    
    CGImageRef newCGImage = CGBitmapContextCreateImage(context);
    UIImage* newImage = [UIImage imageWithCGImage:newCGImage];
    
    CGContextRelease(context);
    CGImageRelease(newCGImage);
    
    return newImage;
}

+ (UIImage *)imageWithCircleOfColor:(UIColor *)color andSize:(CGSize)size
{
    CGRect imageRect = CGRectMake(0, 0, size.width, size.height);
    CGContextRef context = CGBitmapContextCreate(NULL, imageRect.size.width, imageRect.size.height, 8, 0, CGColorSpaceCreateDeviceRGB(), kCGBitmapAlphaInfoMask & kCGImageAlphaPremultipliedLast);
    
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextAddEllipseInRect(context, imageRect);
    CGContextFillPath(context);
    
    CGImageRef newCGImage = CGBitmapContextCreateImage(context);
    UIImage* newImage = [UIImage imageWithCGImage:newCGImage];
    
    CGContextRelease(context);
    CGImageRelease(newCGImage);
    
    return newImage;
}


@end
