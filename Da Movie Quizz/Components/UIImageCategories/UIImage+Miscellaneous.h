//
//  UIImage+CaptainDash.h
//  CaptainDash
//
//  Created by Emilien on 8/23/12.
//  Copyright (c) 2012 CaptainDash. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Miscellaneous)

+ (NSString *)splashImageName;

-(UIImage *)colorizeWhitePartOfImageWithColor:(UIColor *)theColor;
-(UIImage *)colorizeTransparentImageWithColor:(UIColor *)tintColor;
-(UIImage *)colorizeNonTransparentImageWithColor:(UIColor *)tintColor;
-(UIImage *)colorizeAlphaChannelOfImageWithColor:(UIColor *)tintColor;
-(UIImage *)colorizeNonAlphaChannelOfImageWithColor:(UIColor *)tintColor;

-(UIImage*)maskNonTransparentPartOfImageWithColor:(UIColor *)color;

+(UIImage *)imageWithColor:(UIColor *)color;
+ (UIImage *)imageWithSolidColor:(UIColor *)color;

+ (UIImage *)imageWithCircleOfColor:(UIColor *)color andSize:(CGSize)size;

@end
