//
//  Actor.swift
//  Da Movie Quizz
//
//  Created by jibeex on 1/21/16.
//  Copyright © 2016 Jianbin LIN. All rights reserved.
//

import UIKit

class Actor: NSObject {
    
    var id:Int?
    var name:String?
    var image:String?
    weak var movie:Movie?

}
