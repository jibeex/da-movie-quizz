//
//  HighScoreManger.swift
//  Da Movie Quizz
//
//  Created by jibeex on 1/25/16.
//  Copyright © 2016 Jianbin LIN. All rights reserved.
//

import UIKit
import RealmSwift

class HighScoreManger: NSObject {
    
    static private(set) var highScores:[Score] = []
    
    static func latestScore() -> Score?{
        return HighScoreManger.highScores.sort({ (first, second) -> Bool in
            return first.date.isBeforeDate(second.date)
        }).last
    }
    
    static func loadHighScores(){
        do {
            let realm = try Realm()
            highScores = realm.objects(Score.self).sort({ (first:Score, second:Score) -> Bool in
                return first.isHigherThanScore(second)
            }).map { $0 }
            
        } catch let error {
            print("Cannot load high scores:\(error)")
        }
    }
    
    
    
    /*
    ** Return the rank of the given score if it is inserted into the list of high scores
    ** start from 0
    **/
    static func rankOfScore(score:Score) -> Int{
        
        let count = highScores.count
        
        if count > 0{
            for index in 0...(count - 1){
                
                if score.isHigherThanScore(highScores[index]){
                    return index
                }
            }
        }
        
        return highScores.count
    }
    
    
    static func isHighScore(score:Score) -> Bool{
        return score.score > 0 && rankOfScore(score) < 10
    }
    
    static func addHighScore(score:Score){
        
        do {
            let realm = try Realm()
            let rank = rankOfScore(score)
            
            if rank < 10{
                highScores.insert(score, atIndex: rank)
                realm.beginWrite()
                realm.add(score)
                try realm.commitWrite()
            }
            
            if highScores.count > 10{
                realm.beginWrite()
                realm.delete(highScores.last!)
                highScores.removeLast()
                try realm.commitWrite()
            }
        } catch _ {
            print("Cannot delete objects")
        }
    }
    
    static func resetAllScores(){
        do {
            let realm = try Realm()
            realm.beginWrite()
            realm.delete(self.highScores)
            try realm.commitWrite()
            self.highScores = []
        } catch _ {
            print("Cannot delete objects")
        }
    }

}
