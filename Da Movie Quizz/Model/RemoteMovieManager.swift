//
//  RemoteMovieManager.swift
//  Da Movie Quizz
//
//  Created by jibeex on 1/23/16.
//  Copyright © 2016 Jianbin LIN. All rights reserved.
//

import UIKit

class RemoteMovieManager: NSObject {
    
    static var imagesBaseUrlString:String!
    
    static func loadConfiguration(didFinish:((error:NSError?) -> Void)?) {
        
        JLTMDbClient.sharedAPIInstance().GET(kJLTMDbConfiguration, withParameters: nil) { (response, error) -> Void in
            if error == nil {
                if let response = response as? [String:AnyObject]{
                    if let imagesConfigurations = response["images"] as? [String:AnyObject]{
                        if let baseUrl = imagesConfigurations["base_url"] as? String{
                            self.imagesBaseUrlString = baseUrl + "w185"
                            didFinish?(error: nil)
                        }
                    }
                }
            }
            else {
                didFinish?(error: error)
            }
        }
    }
    
    static func loadMovies(didFinish:((movies:[Movie]?, error:NSError?) -> Void)?){
        var optionsArray = [kJLTMDbMoviePopular, kJLTMDbMovieUpcoming, kJLTMDbMovieTopRated]
        JLTMDbClient.sharedAPIInstance().GET(optionsArray[random() % optionsArray.count], withParameters: nil, andResponseBlock: {
            (response, error) -> Void in
            if error == nil {
                
                if let response = response as? [String:AnyObject]{
                    
                    var movies:[Movie] = []
                    
                    if let results = response["results"] as? [[String:AnyObject]]{
                        
                        let numberOfMovies = results.count
                        var numberOfMoviesWithCastLoaded = 0
                        
                        for m in results{
                            let movie = Movie()
                            
                            if let id = m["id"] as? Int{
                                movie.id = id
                            }
                            
                            if let title = m["title"] as? String{
                                movie.name = title
                            }
                            
                            if let poster = m["poster_path"] as? String{
                                movie.image = poster
                            }
                            
                            movies.append(movie)
                            movie.loadCast({ (error) -> Void in
                                
                                let delayTime = dispatch_time(DISPATCH_TIME_NOW,
                                    Int64(0 * Double(NSEC_PER_SEC)))
                                
                                dispatch_after(delayTime, dispatch_get_main_queue(), {
                                    ++numberOfMoviesWithCastLoaded
                                    if numberOfMoviesWithCastLoaded >= numberOfMovies{
                                        
                                        // Discard movie that doesn't have cast or image
                                        let moviesHavingCast = movies.filter({ (movie) -> Bool in
                                            movie.cast.count > 0 && movie.image != nil
                                        })
                                        didFinish?(movies: moviesHavingCast, error: nil)
                                    }
                                })
                                
                            })
                            
                        }
                    }
                    return
                }
            }
            
            didFinish?(movies: nil, error: error)
        })
    }

}
