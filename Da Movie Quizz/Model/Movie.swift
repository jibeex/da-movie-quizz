//
//  Film.swift
//  Da Movie Quizz
//
//  Created by jibeex on 1/21/16.
//  Copyright © 2016 Jianbin LIN. All rights reserved.
//

import UIKit

class Movie: NSObject {
    
    var id:Int?
    var name:String?
    var image:String?
    var cast:[Actor] = []
    
    
    func loadCast(didFinish:((error:NSError?) -> Void)?){
        
        if let id = self.id{
            JLTMDbClient.sharedAPIInstance().GET(kJLTMDbMovieCasts, withParameters: ["id":"\(id)"], andResponseBlock: {
                (response, error) -> Void in
                if error == nil {
                    
                    if let response = response as? [String:AnyObject]{
                        
                        var cast:[Actor] = []
                        var error:NSError?
                        
                        if let results = response["cast"] as? [[String:AnyObject]]{
                            
                            if results.count > 0{
                                for (index, r) in results.enumerate(){
                                    
                                    // load only famous actors
                                    if index > 5{
                                        break
                                    }
                                    
                                    let actor = Actor()
                                    
                                    if let id = r["id"] as? Int{
                                        actor.id = id
                                    }
                                    
                                    if let name = r["name"] as? String{
                                        actor.name = name
                                    }
                                    
                                    if let image = r["profile_path"] as? String{
                                        actor.image = image
                                        actor.movie = self
                                        cast.append(actor)
                                        self.cast = cast
                                    }
                                }
                            }
                            else{
                                error = NSError(domain: "app", code: 1, userInfo: [NSLocalizedDescriptionKey : "Cast list is empty"])
                            }
                        }
                        else{
                            error = NSError(domain: "app", code: 2, userInfo: [NSLocalizedDescriptionKey : "Cast not found"])
                        }
                        
                        didFinish?(error: error)
                    }
                }
                else{
                    didFinish?(error: error)
                }
                
            })
        }
        else{
            let error = NSError(domain: "app", code: 0, userInfo: [NSLocalizedDescriptionKey : "id of movie should not be nil"])
            didFinish?(error: error)
        }
        
    }

}
