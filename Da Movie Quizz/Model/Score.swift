//
//  Score.swift
//  Da Movie Quizz
//
//  Created by jibeex on 1/21/16.
//  Copyright © 2016 Jianbin LIN. All rights reserved.
//

import UIKit
import RealmSwift

class Score: Object {
    
    dynamic var playerName = ""
    dynamic var time:Double = 0
    dynamic var score = 0
    dynamic var date:NSDate = NSDate()
    
    
    func isHigherThanScore(another:Score) -> Bool{
        
        if self.score > another.score{
            return true
        }
        else if self.score == another.score{
            if self.time < another.time{
                return true
            }
            else if self.time == another.time{
                if self.date.isBeforeDate(another.date){
                    return true
                }
            }
        }
        
        return false
    }
}
