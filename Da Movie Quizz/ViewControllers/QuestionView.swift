import UIKit

class QuestionView: UIView {
    let imageMarginSpace: CGFloat = 5.0
    var questionField: UITextView!
    var animator: UIDynamicAnimator!
    var originalCenter: CGPoint!
    var question: (movie:Movie, actor:Actor, result:Bool)!
    var answer: Bool!
    
    @IBOutlet var actorImage:UIImageView!
    @IBOutlet var movieImage:UIImageView!
    @IBOutlet var stampYesImage:UIImageView!
    @IBOutlet var stampNoImage:UIImageView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
    }
    
    func setUpQuestion(question:(movie:Movie, actor:Actor, result:Bool)){
        
        self.answer = question.result
        self.question = question
        
        // Apple thing. For physics
        animator = UIDynamicAnimator(referenceView: self)
    }
    
    func viewWillBecomeActive(){
        
        // load images
        if let image = question.actor.image{
            let imageUrl = RemoteMovieManager.imagesBaseUrlString + image
            self.actorImage.setImageWithURL(imageUrl, fadeIn: true)
            print("Load image:\(imageUrl)\n")
        }
        else{
            print("No actor image\n")
        }
        
        if let image = question.movie.image{
            let movieUrl = RemoteMovieManager.imagesBaseUrlString + image
            self.movieImage.setImageWithURL(movieUrl, fadeIn: true)
            print("Load image:\(movieUrl)\n")
        }
        else{
            print("No movie image\n")
        }
        
        self.applyShadow()
        self.stampYesImage.alpha = 0
        self.stampNoImage.alpha = 0
        
        self.stampYesImage.transform = CGAffineTransformMakeRotation(-CGFloat(M_PI_4))
        self.stampNoImage.transform = CGAffineTransformMakeRotation(CGFloat(M_PI_4))
    }
    
    func applyShadow() {
        self.layer.cornerRadius = 6.0
        self.layer.shadowColor = UIColor.blackColor().CGColor
        self.layer.shadowOpacity = 0.3
        self.layer.shadowOffset = CGSize(width: 0, height: -3)
    }
    
    func swipe(answer: Bool) {
        animator.removeAllBehaviors()
        
        // If the answer is false, Move to the left
        // Else if the answer is true, move to the right
        let gravityX = answer ? 0.5 : -0.5
        let magnitude = answer ? 20.0 : -20.0
        let gravityBehavior:UIGravityBehavior = UIGravityBehavior(items: [self])
        gravityBehavior.gravityDirection = CGVectorMake(CGFloat(gravityX), 0)
        animator.addBehavior(gravityBehavior)
        
        let pushBehavior:UIPushBehavior = UIPushBehavior(items: [self], mode: UIPushBehaviorMode.Instantaneous)
        pushBehavior.magnitude = CGFloat(magnitude)
        animator.addBehavior(pushBehavior)
        
    }
    
    func panProgress(answer: Bool, progress:Float) {
        
        let stampAlpha =  CGFloat(min(max(0, (progress - 0.05) * 4), 1))
        
        if answer{
            self.stampYesImage.alpha = stampAlpha
        }
        else{
            self.stampNoImage.alpha = stampAlpha
        }
    }
    
    func returnToCenter() {
        UIView.animateWithDuration(0.8, delay: 0.1, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .AllowUserInteraction, animations: {
            self.center = self.originalCenter
            }, completion: { finished in
            }
        )
        
        UIView.animateWithDuration(0.3, delay: 0.1, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .AllowUserInteraction, animations: {
            self.stampYesImage.alpha = 0
            self.stampNoImage.alpha = 0
            }, completion: { finished in
            }
        )
        
    }

}
