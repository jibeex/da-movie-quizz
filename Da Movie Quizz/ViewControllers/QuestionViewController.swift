import UIKit

class QuestionViewController: UIViewController {
    
    var score: Int!
    var done: Bool = false
    
    var questions:[(movie:Movie, actor:Actor, result:Bool)]!
    
    @IBOutlet var timeLabel:UILabel!
    @IBOutlet var highScoreLabel: UILabel!
    @IBOutlet var scoreLabel: UILabel!
    @IBOutlet var questionLabel:UILabel!
    @IBOutlet var questionImagesContainer:UIView!
    
    private var timer:NSTimer!
    private var startDate:NSDate!
    private var timeElapsed:Double!
    
    let gameOverVC = GameOverVC(nibName: "GameOverVC")
    
    
    @IBAction func truePressed(sender: AnyObject) {
        self.determineJudgement(true)
    }
    
    @IBAction func falsePressed(sender: AnyObject) {
        self.determineJudgement(false)
    }
    
    var questionViews: [QuestionView] = []
    var currentQuestionView: QuestionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Preload game over VC
        self.addChildViewController(self.gameOverVC)
        
        // Start with a 0 score
        score = 0
        
        for question in self.questions {
            currentQuestionView = NSBundle.mainBundle().loadNibNamed("QuestionView", owner: self, options: nil).first as! QuestionView
            currentQuestionView.frame = self.questionImagesContainer.bounds
            currentQuestionView.originalCenter = currentQuestionView.center
            currentQuestionView.setUpQuestion(question)
            self.questionViews.append(currentQuestionView)
        }
        
        for questionView in self.questionViews {
            self.questionImagesContainer.addSubview(questionView)
        }
       
        // Add Pan Gesture Recognizer
        let pan = UIPanGestureRecognizer(target: self, action: Selector("handlePan:"))
        self.view.addGestureRecognizer(pan)
        
        self.scoreLabel.text = "SCORE: 0"
        
        if let highScore = HighScoreManger.highScores.first{
            self.highScoreLabel.text = "HIGH SCORE:\(highScore.score)"
        }
        else{
            self.highScoreLabel.hidden = true
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        // load last and last - 1 question view
        
        if questionViews.count > 0{
            questionViews.last?.viewWillBecomeActive()
            self.questionLabel.text = self.questionText()
            
        }
        
        if questionViews.count > 1{
            questionViews[questionViews.count - 2].viewWillBecomeActive()
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: "timerAction", userInfo: nil, repeats: true)
        self.startDate = NSDate()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func determineJudgement(answer: Bool) {
        
        // If its the right answer, set the score
        if self.currentQuestionView.answer == answer && !self.done{
            self.score = self.score + 100
            
            
            if let highScore = HighScoreManger.highScores.first{
                
                if self.score > highScore.score{
                    UIView.animateWithDuration(0.2, delay: 0.1, usingSpringWithDamping: 1.0, initialSpringVelocity: 0, options: [], animations: { () -> Void in
                        self.scoreLabel.text = "NEW HIGH SCORE: \(self.score)"
                        self.scoreLabel.transform = CGAffineTransformMakeScale(1.2, 1.2)
                        self.scoreLabel.textColor = UIColor.yellowColor()
                        }, completion: {
                            (complete) in
                            UIView.animateWithDuration(0.25, delay: 0.0, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: { () -> Void in
                                self.scoreLabel.transform = CGAffineTransformIdentity
                                self.scoreLabel.textColor = UIColor.redColor()
                                }, completion: nil)
                            
                    })
                }
                else{
                    self.scoreLabel.text = "SCORE: \(self.score)"
                }
                
            }
            else{
                self.scoreLabel.text = "SCORE: \(self.score)"
            }
            
        }
        else{
            self.displayGameOverView()
        }
        
        // Run the swipe animation
        self.currentQuestionView.swipe(answer)
        
        // Handle when we have no more matches
        self.questionViews.removeAtIndex(self.questionViews.count - 1)
        self.questions.removeAtIndex(self.questions.count - 1)
        
        // Set the new current question to the next one
        self.currentQuestionView = self.questionViews.last!
        self.questionLabel.text = self.questionText()
        
        if questionViews.count > 1{
            questionViews[questionViews.count - 2].viewWillBecomeActive()
        }
        
    }
    
    func handlePan(gesture: UIPanGestureRecognizer) {
        // Is this gesture state finished??
        
        let questionViewRelativePosition = self.currentQuestionView.center.x / self.view.bounds.maxX
        
        if gesture.state == UIGestureRecognizerState.Ended {
            // Determine if we need to swipe off or return to center
            _ = gesture.locationInView(self.view)
            if questionViewRelativePosition > 0.8 {
                self.determineJudgement(true)
            }
            else if questionViewRelativePosition < 0.2 {
                self.determineJudgement(false)
            }
            else {
                self.currentQuestionView.returnToCenter()
            }
        }
        else{
            if questionViewRelativePosition > 0.5{
                self.currentQuestionView.panProgress(true, progress: Float((questionViewRelativePosition - 0.5))/0.5)
            }
            else if questionViewRelativePosition < 0.5{
                self.currentQuestionView.panProgress(false, progress: Float((0.5 - questionViewRelativePosition))/0.5)
            }
        }
        
        
        let translation = gesture.translationInView(self.currentQuestionView)
        self.currentQuestionView.center = CGPoint(x: self.currentQuestionView!.center.x + translation.x, y: self.currentQuestionView!.center.y + translation.y)
        gesture.setTranslation(CGPointZero, inView: self.view)
    }
    
    
    // called every time interval from the timer
    func timerAction() {
        self.timeElapsed = NSDate().timeIntervalSinceDate(self.startDate)
        self.timeLabel.text = String(format: "TIME: %.1f s", timeElapsed)
    }
    
    func displayGameOverView(){
        
        dispatch_after(0, dispatch_get_main_queue(), {
            let score = Score()
            score.score = self.score
            score.time = self.timeElapsed
            self.gameOverVC.score = score
            self.gameOverVC.displayInView(self.view)
        })
        
    }
    
    func questionText() -> String?{
        
        if let question = self.questions.last{
            
            if let actorName = question.actor.name,
                let movieName = question.movie.name{
                    return "Does \(actorName) perform in\n[\(movieName)] ?"
            }
        }
        
        return nil
    }
}
