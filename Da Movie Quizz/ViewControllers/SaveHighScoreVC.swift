//
//  SaveHighScoreVC.swift
//  Da Movie Quizz
//
//  Created by jibeex on 1/25/16.
//  Copyright © 2016 Jianbin LIN. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding

class SaveHighScoreVC: UIViewController, UITableViewDataSource {
    
    @IBOutlet private var playerNameTextField:UITextField!
    @IBOutlet private var tableView:UITableView!
    @IBOutlet private var contentView:UIView!
    
    @IBOutlet private var prompt:UILabel!
    
    private var highScores:[Score] = HighScoreManger.highScores
    
    private var score:Score!
    
    init(score:Score, nibName: String?) {
        super.init(nibName: nibName, bundle: nil)
        self.score = score
        highScores.append(self.score)
        highScores.sortInPlace { (first, second) -> Bool in
            return first.isHigherThanScore(second)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.registerNib(UINib(nibName: "HighScoreCell", bundle: nil), forCellReuseIdentifier: "HighScoreCell")
        
        IHKeyboardAvoiding.setAvoidingView(self.contentView)
        self.hidePrompt()
        
        // Preload latest user name
        if let latestScore = HighScoreManger.latestScore(){
            playerNameTextField.text = latestScore.playerName
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - UITableView data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.highScores.count + 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        let cell = self.tableView.dequeueReusableCellWithIdentifier("HighScoreCell") as! HighScoreCell
        var textColor = UIColor.whiteColor()
        
        if indexPath.row == 0{
            cell.set("Rank", name: "Player", score: "Score", time: "Time", textColor: textColor)
        }
        else{
            let cellScore = self.highScores[indexPath.row - 1]
            let time = NSDate.stringFromTimeInterval(cellScore.time)
            var playerName = cellScore.playerName
            
            if self.score === cellScore{
                textColor = UIColor(hexString: "F53F46")
                playerName = "? ? ? (Your name)"
            }
            cell.set("#\(indexPath.row)", name: playerName, score: "\(cellScore.score)", time: time, textColor: textColor)
        }
        
        return cell
    }

    
    // MARK: - Action Handlers
    
    @IBAction func saveButtonDidPress(){
        
        
        if let playerName = self.playerNameTextField.text{
            
            let regex = try! NSRegularExpression(pattern: ".*[^A-Za-z0-9\\s-_'\\.].*", options: [])
            if regex.firstMatchInString(playerName, options: [], range: NSMakeRange(0, playerName.characters.count)) != nil {
                self.showPrompt("Please enter a valid name. Usernames can contain letters (a-z), numbers (0-9), dashes (-), underscores (_), apostrophes ('}, and periods (.)")
                
            }
            else if playerName.stringByReplacingOccurrencesOfString(" ", withString: "").lengthOfBytesUsingEncoding(NSUTF8StringEncoding) > 0{
                self.score.playerName = playerName
                HighScoreManger.addHighScore(self.score)
                self.dismissViewControllerAnimated(true) { () -> Void in
                    
                }
            }
            else{
                self.showPrompt("Player field cannot be empty")
            }
        }
        
        
        
    }
    
    @IBAction func cancelButtonDidPress(){
        self.dismissViewControllerAnimated(true) { () -> Void in
            
        }
    }
    
    @IBAction func closeKeyboard(){
        self.view.endEditing(true)
    }
    
    
    // MARK: - Prompt
    func hidePrompt(){
        UIView.animateWithDuration(0.2, delay: 0, options: UIViewAnimationOptions.CurveLinear, animations: { () -> Void in
            self.prompt.alpha = 0
            }) { (complete) -> Void in
        }
    }
    
    func showPrompt(message:String){
        self.prompt.text = message
        UIView.animateWithDuration(0.2, delay: 0, options: UIViewAnimationOptions.CurveEaseOut, animations: { () -> Void in
            self.prompt.alpha = 1
            }) { (complete) -> Void in
                self.prompt.vibrate(20, duration: 0.07, repeatCount: 4)
        }
    }

}
