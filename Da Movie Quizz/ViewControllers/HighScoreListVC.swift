//
//  SaveHighScoreVC.swift
//  Da Movie Quizz
//
//  Created by jibeex on 1/25/16.
//  Copyright © 2016 Jianbin LIN. All rights reserved.
//

import UIKit
import SCLAlertView

class HighScoreListVC: UIViewController, UITableViewDataSource {
    
    @IBOutlet private var tableView:UITableView!
    @IBOutlet private var contentView:UIView!
    
    @IBOutlet private var prompt:UILabel!
    
    private var highScores:[Score]{
        get{
            return HighScoreManger.highScores
        }
    }
    
    init(nibName: String?) {
        super.init(nibName: nibName, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.registerNib(UINib(nibName: "HighScoreCell", bundle: nil), forCellReuseIdentifier: "HighScoreCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - UITableView data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.highScores.count + 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        let cell = self.tableView.dequeueReusableCellWithIdentifier("HighScoreCell") as! HighScoreCell
        let textColor = UIColor.whiteColor()
        
        if indexPath.row == 0{
            cell.set("Rank", name: "Player", score: "Score", time: "Time", textColor: textColor)
        }
        else{
            let cellScore = self.highScores[indexPath.row - 1]
            let playerName = cellScore.playerName
            let time = NSDate.stringFromTimeInterval(cellScore.time)
            cell.set("#\(indexPath.row)", name: playerName, score: "\(cellScore.score)", time: time, textColor: textColor)
        }
        
        
        
        return cell
    }
    
    
    // MARK: - Action Handlers
    
    @IBAction func resetButtonDidPress(){
        
        let alertView = SCLAlertView()
        alertView.addButton("YES") {
            HighScoreManger.resetAllScores()
            self.tableView.reloadData()
        }
        alertView.addButton("NO") {
        }
        alertView.showCloseButton = false
        alertView.showWarning("Warning", subTitle: "Are you sure to reset all high scores?")
        
    }
    
    @IBAction func closeButtonDidPress(){
        self.dismissViewControllerAnimated(true) { () -> Void in
            
        }
    }

}
