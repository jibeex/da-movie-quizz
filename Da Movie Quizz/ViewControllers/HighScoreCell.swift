//
//  HighScoreCell.swift
//  Da Movie Quizz
//
//  Created by jibeex on 1/25/16.
//  Copyright © 2016 Jianbin LIN. All rights reserved.
//

import UIKit

class HighScoreCell: UITableViewCell {
    
    @IBOutlet var rankLabel:UILabel!
    @IBOutlet var nameLabel:UILabel!
    @IBOutlet var scoreLabel:UILabel!
    @IBOutlet var timeLabel:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func set(rank:String, name:String, score:String, time:String, textColor:UIColor){
        rankLabel.text = rank
        rankLabel.textColor = textColor
        nameLabel.text = name
        nameLabel.textColor = textColor
        scoreLabel.text = score
        scoreLabel.textColor = textColor
        timeLabel.text = time
        timeLabel.textColor = textColor
    }
    
}
