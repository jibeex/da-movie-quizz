//
//  GameOverVC.swift
//  Da Movie Quizz
//
//  Created by jibeex on 1/25/16.
//  Copyright © 2016 Jianbin LIN. All rights reserved.
//

import UIKit

class GameOverVC: UIViewController {
    
    private var blurView:FXBlurView!
    var score:Score!
    
    @IBOutlet var scoreLabel:UILabel!
    @IBOutlet var timeLabel:UILabel!
    
    private var highScoreIsChecked:Bool = false
    
    init(nibName: String?) {
        super.init(nibName: nibName, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        self.scoreLabel.text = "\(self.score.score)"
        self.timeLabel.text = NSDate.stringFromTimeInterval(self.score.time)
        
        if !self.highScoreIsChecked{
            self.highScoreIsChecked = true
            if HighScoreManger.isHighScore(self.score){
                let delayTime = dispatch_time(DISPATCH_TIME_NOW,
                    Int64(0.0 * Double(NSEC_PER_SEC)))
                
                dispatch_after(delayTime, dispatch_get_main_queue(), {
                    let highScoreVC = SaveHighScoreVC(score: self.score, nibName: "SaveHighScoreVC")
                    self.parentViewController?.presentViewController(highScoreVC, animated: true, completion: { () -> Void in
                        
                    })
                })
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func displayInView(container:UIView){
        
        self.blurView = FXBlurView(frame: container.bounds)
        blurView.tintColor = UIColor.clearColor()
        blurView.dynamic = false
        blurView.autoresizingMask = [.FlexibleHeight, .FlexibleWidth]
        container.addSubview(blurView)
        
        self.view.alpha = 0
        
        self.view.frame = container.bounds
        
        self.view.autoresizingMask = [.FlexibleHeight, .FlexibleWidth]
        container.addSubview(self.view)
        self.view.transform = CGAffineTransformScale(self.view.transform, 1.5, 1.5)
        
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.view.transform = CGAffineTransformScale(self.view.transform, 0.666, 0.666)
            self.view.alpha = 1
            self.blurView.blurRadius = 15
            }) { (complete) -> Void in
        }
    }
    
    func dismiss(){
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.view.transform = CGAffineTransformScale(self.view.transform, 1.5, 1.5)
            self.view.alpha = 0
            self.blurView.alpha = 0
            }) { (complete) -> Void in
                self.view.removeFromSuperview()
                self.blurView.removeFromSuperview()
                self.blurView = nil
                self.removeFromParentViewController()
        }
    }
    
    // MARK: - Action handlers
    
    @IBAction func replayButtonDidPress(){
        self.parentViewController?.dismissViewControllerAnimated(true, completion: { () -> Void in
            StartViewController.sharedInstance.startButtonDidPress()
        })
    }
    
    @IBAction func quitButtonDidPress(){
        self.parentViewController?.dismissViewControllerAnimated(true, completion: { () -> Void in
        })
    }

}
