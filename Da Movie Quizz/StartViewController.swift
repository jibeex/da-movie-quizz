//
//  ViewController.swift
//  Da Movie Quizz
//
//  Created by jibeex on 1/21/16.
//  Copyright © 2016 Jianbin LIN. All rights reserved.
//

import UIKit
import JHSpinner

class StartViewController: UIViewController {
    
    static var sharedInstance:StartViewController!
    
    @IBOutlet var questionVC:UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        StartViewController.sharedInstance = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func composeQuestions(numberOfQuestions:Int, movies:[Movie]) -> [(movie:Movie, actor:Actor, result:Bool)]{
        
        var ret:[(movie:Movie, actor:Actor, result:Bool)] = []
        
        repeat{
            
            var question:(movie:Movie, actor:Actor, result:Bool)
            
            let movie = movies[ret.count % movies.count]
            
            let actor:Actor
            
            //choose randomly if the answer is true or false
            let result = random() % 2 == 0
            
            if result{
                actor = movie.cast[random() % movie.cast.count]
            }
            else{
                // chose another movie
                var anotherMovie:Movie
                repeat{
                    anotherMovie = movies[random() % movies.count]
                }
                    while(anotherMovie == movie)
                actor = anotherMovie.cast[random() % anotherMovie.cast.count]
                
                // check if this actor is also playing the current movie
                if movie.cast.filter({ (currentActor) -> Bool in
                    actor.id == currentActor.id
                }).count > 0{
                    continue
                }
            }
            
            question = (movie: movie, actor: actor, result: result)
            
            //check if this question is duplicated
            if ret.filter({ (tuple: (movie: Movie, actor: Actor, result: Bool)) -> Bool in
                if tuple.movie === question.movie && tuple.actor === question.actor{
                    return true
                }
                else{
                    return false
                }
            }).count == 0{
                ret.append(question)
                
            }
        }
        while ret.count < numberOfQuestions
        
        return ret
        
    }
    
    
    @IBAction func startButtonDidPress(){
        
        
        //mask.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.2)
        let blurView = FXBlurView(frame: self.view.bounds)
        blurView.tintColor = UIColor.blackColor().colorWithAlphaComponent(0.1)
        blurView.dynamic = false
        blurView.blurRadius = 10
        blurView.autoresizingMask = [.FlexibleHeight, .FlexibleWidth]
        //mask.addSubview(blurView)
        let mask = blurView
        self.view.addSubview(mask)
        let spinner = JHSpinnerView.showOnView(mask, spinnerColor:UIColor.redColor(), overlay:.Circular, overlayColor:UIColor.blackColor().colorWithAlphaComponent(0.3))
        mask.addSubview(spinner)
        
        mask.alpha = 0
        UIView.animateWithDuration(0.3, animations: { () -> Void in
            mask.alpha = 1
            }) { (complete) -> Void in
                RemoteMovieManager.loadConfiguration { (error) -> Void in
                    if error == nil{
                        RemoteMovieManager.loadMovies({ (movies, error) -> Void in
                            
                            if let movies = movies{
                                let questions = self.composeQuestions(100, movies: movies)
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewControllerWithIdentifier("QuestionViewController") as! QuestionViewController
                                vc.questions = questions
                                self.presentViewController(vc, animated: true, completion: nil)
                                spinner.dismiss()
                                mask.removeFromSuperview()
                            }
                            
                        })
                    }
                }
                
        }
    }
    
    @IBAction func highScoresButtonDidPress(){
        
        let vc = HighScoreListVC(nibName: "HighScoreListVC")
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    
}

