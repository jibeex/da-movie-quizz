//
//  Globals.swift
//  Da Movie Quizz
//
//  Created by jibeex on 1/23/16.
//  Copyright © 2016 Jianbin LIN. All rights reserved.
//

import UIKit

func Localized(key:String?) -> String!{
    if let k = key{
        return NSLocalizedString(k, comment: "")
    }
    return nil
}
